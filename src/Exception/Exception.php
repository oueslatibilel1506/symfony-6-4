<?php

declare(strict_types=1);

namespace App\Exception;

abstract class Exception extends \Exception
{
    public const DEFAULT_TITLE = 'An error occurred';

    public const DEFAULT_MESSAGE = 'We are sorry for the inconvenience and thank you for your understanding.';

    protected ?iterable $errors = null;

    protected string $technicalMessage;

    protected string $title;

    public function getErrors(): ?iterable
    {
        return $this->errors;
    }

    public function setErrors(?iterable $errors): void
    {
        $this->errors = $errors;
    }

    public function getTechnicalMessage(): string
    {
        return $this->technicalMessage;
    }

    public function setTechnicalMessage(string $technicalMessage): void
    {
        $this->technicalMessage = $technicalMessage;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }
}
