<?php

declare(strict_types=1);

namespace App\Exception;

class FactoryException extends Exception {}
