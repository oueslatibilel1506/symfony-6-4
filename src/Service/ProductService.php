<?php

declare(strict_types=1);

namespace App\Service;

use App\Repository\ProductRepository;

class ProductService extends EntityService
{
    public function __construct(
        private readonly ProductRepository $productRepository,
    ) {
        parent::__construct($this->productRepository);
    }
}
