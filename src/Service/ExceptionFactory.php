<?php

declare(strict_types=1);

namespace App\Service;

use App\Exception\Exception;
use App\Exception\FactoryException;
use Symfony\Component\HttpFoundation\Response;

class ExceptionFactory
{
    public function create(
        string $className,
        int $code = Response::HTTP_NOT_FOUND,
        string $technicalMessage = '',
        string $title = '',
        string $message = '',
        iterable $errors = []
    ): Exception {
        if (true === is_subclass_of($className, Exception::class)) {
            /** @var Exception $exception */
            $exception = new $className($message, $code);
            $exception->setTitle($title);
            $exception->setTechnicalMessage($technicalMessage);
            $exception->setErrors($errors);

            return $exception;
        }

        return $this->create(
            FactoryException::class,
            Response::HTTP_NOT_FOUND,
            "{$className} Exception not found",
            Exception::DEFAULT_TITLE,
            Exception::DEFAULT_MESSAGE
        );
    }
}
