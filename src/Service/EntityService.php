<?php

declare(strict_types=1);

namespace App\Service;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class EntityService
{
    public function __construct(
        private readonly ServiceEntityRepository $repository,
    ) {}

    /**
     * @throws \Exception
     */
    public function findById(int $id)
    {
        $entity = $this->repository->find($id);

        // Ensure the entity exists in persistence
        if (is_null($entity)) {
            throw new NotFoundHttpException('No entity found for id '.$id);
        }

        return $entity;
    }
}
