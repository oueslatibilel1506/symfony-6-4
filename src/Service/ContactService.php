<?php

declare(strict_types=1);

namespace App\Service;

use App\Repository\ContactRepository;

class ContactService extends EntityService
{
    public function __construct(
        private readonly ContactRepository $contactRepository,
    ) {
        parent::__construct($this->contactRepository);
    }
}
