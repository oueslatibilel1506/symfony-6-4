<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Subscription;
use App\Exception\Exception;
use App\Repository\SubscriptionRepository;

class SubscriptionService extends EntityService
{
    public function __construct(
        private readonly SubscriptionRepository $subscriptionRepository,
    ) {
        parent::__construct($this->subscriptionRepository);
    }

    /**
     * @throws Exception
     */
    public function save(Subscription &$subscription): void
    {
        $this->subscriptionRepository->save($subscription);
    }

    /**
     * @throws Exception
     */
    public function remove(Subscription $subscription): void
    {
        $this->subscriptionRepository->remove($subscription);
    }
}
