<?php

namespace App\DataFixtures;

use App\Entity\Contact;
use App\Entity\Product;
use App\Entity\Subscription;
use App\Factory\ContactFactory;
use App\Factory\ProductFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory as FakerFactory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $contactProxies = ContactFactory::createMany(3);
        $productProxies = ProductFactory::createMany(100);

        // Initialiser Faker pour générer des données aléatoires
        $faker = FakerFactory::create();

        $contact = reset($contactProxies);
        $product = reset($productProxies);
        if ($contact instanceof Contact
            && $product instanceof Product
        ) {
            $subscription = (new Subscription())
                ->setContact($contact)
                ->setProduct($product)
                ->setBeginDate($faker->dateTimeBetween('-1 year', 'now'))
                ->setEndDate($faker->dateTimeBetween('now', '+1 year'))
            ;

            $manager->persist($subscription);
            $manager->flush();
        }
    }
}
