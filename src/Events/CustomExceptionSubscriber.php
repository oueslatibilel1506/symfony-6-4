<?php

declare(strict_types=1);

namespace App\Events;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

/**
 * Turn some of our model exceptions into HTTP exceptions.
 */
#[AsEventListener]
class CustomExceptionSubscriber
{
    public function __construct(private readonly LoggerInterface $logger) {}

    public function __invoke(ExceptionEvent $event): void
    {
        // The isMainRequest() method was introduced in Symfony 5.3.
        // In previous versions it was called isMasterRequest()
        if (!$event->isMainRequest()) {
            // don't do anything if it's not the main request
            return;
        }

        // You get the exception object from the received event
        $exception = $event->getThrowable();

        $message = [
            'message' => $exception->getMessage(),
        ];

        // Customize your response object to display the exception details
        $response = new Response();
        $response->setContent(json_encode($message) ? json_encode($message) : '');

        // HttpExceptionInterface is a special type of exception that
        // holds status code and header details
        if ($exception instanceof HttpExceptionInterface) {
            $response->setStatusCode($exception->getStatusCode());
            $response->headers->replace($exception->getHeaders());

            // Log Error
            if ($exception->getStatusCode() >= Response::HTTP_INTERNAL_SERVER_ERROR) {
                $this->logger->error(
                    $exception->getMessage(),
                    [
                        'exception' => $exception,
                    ]
                );
            }
        } else {
            $statusCode = $exception->getCode();
            if (0 == $statusCode) {
                $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
            }

            $response->setStatusCode($statusCode);
        }

        $response->headers->set('Content-Type', 'application/json');

        // sends the modified response object to the event
        $event->setResponse($response);
    }
}
