<?php

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class SubscriptionDto
{
    public function __construct(
        #[Assert\Type('integer')]
        public int $idContact,
        #[Assert\Type('integer')]
        public int $idProduct,
        #[Assert\NotBlank]
        public \DateTimeInterface $beginDate,
        #[Assert\NotBlank]
        public \DateTimeInterface $endDate,
    ) {}

    public function getBeginDate(): \DateTimeInterface
    {
        return $this->beginDate;
    }

    public function getEndDate(): \DateTimeInterface
    {
        return $this->endDate;
    }

    public function getIdContact(): int
    {
        return $this->idContact;
    }

    public function getIdProduct(): int
    {
        return $this->idProduct;
    }
}
