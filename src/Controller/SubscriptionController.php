<?php

namespace App\Controller;

use App\DTO\SubscriptionDto;
use App\Entity\Contact;
use App\Entity\Subscription;
use App\Service\ContactService;
use App\Service\ProductService;
use App\Service\SubscriptionService;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api/subscription')]
#[OA\Tag(name: 'Subscription')]
class SubscriptionController extends AbstractController
{
    public function __construct(
        private readonly ContactService $contactService,
        private readonly ProductService $productService,
        private readonly SubscriptionService $subscriptionService,
        private readonly SerializerInterface $serializer
    ) {}

    /**
     * @throws \Exception
     */
    #[Route('', name: 'add_subscription', methods: 'POST')]
    #[OA\Response(
        response: Response::HTTP_CREATED,
        description: 'Subscription created successfully.',
    ), OA\Response(
        response: Response::HTTP_NOT_FOUND,
        description: 'Resource not found.'
    ), OA\Response(
        response: Response::HTTP_UNPROCESSABLE_ENTITY,
        description: 'Unprocessable Content.'
    ),]
    public function createSubscription(
        #[MapRequestPayload]
        SubscriptionDto $subscriptionDto
    ): JsonResponse {
        $this->createSubscriptionFromDto($subscriptionDto);

        return $this->json([], Response::HTTP_CREATED);
    }

    /**
     * @throws \Exception
     */
    #[Route('/{idSubscription}', name: 'put_sub', requirements: ['idSubscription' => '\d+'], methods: 'PUT')]
    #[
        OA\Response(
            response: Response::HTTP_CREATED,
            description: 'Subscription updated successfully.',
        ), OA\Response(
            response: Response::HTTP_NOT_FOUND,
            description: 'Resource not found.'
        ), OA\Response(
            response: Response::HTTP_UNPROCESSABLE_ENTITY,
            description: 'Unprocessable Content.'
        ),
    ]
    public function putSubscription(
        int $idSubscription,
        #[MapRequestPayload]
        SubscriptionDto $subscriptionDto,
        #[MapEntity(id: 'idSubscription')]
        ?Subscription $subscription = null
    ): JsonResponse {
        if (is_null($subscription)) {
            throw $this->createNotFoundException('No Subscription found for id '.$idSubscription);
        }

        $this->createSubscriptionFromDto($subscriptionDto, $subscription);

        return $this->json([], Response::HTTP_NO_CONTENT);
    }

    /**
     * @throws \App\Exception\Exception
     */
    #[Route('/{idSubscription}', name: 'remove_sub', requirements: ['idSubscription' => '\d+'], methods: 'DELETE')]
    #[
        OA\Response(
            response: Response::HTTP_CREATED,
            description: 'Subscription updated successfully.',
        ), OA\Response(
            response: Response::HTTP_NOT_FOUND,
            description: 'Resource not found.'
        ),
    ]
    public function deleteSubscription(
        int $idSubscription,
        #[MapEntity(id: 'idSubscription')]
        ?Subscription $subscription = null
    ): JsonResponse {
        if (is_null($subscription)) {
            throw $this->createNotFoundException('No Subscription found for id '.$idSubscription);
        }

        $this->subscriptionService->remove($subscription);

        return new JsonResponse([], Response::HTTP_NO_CONTENT);
    }

    #[Route(
        '/{idContact}',
        name: 'get_subscription',
        requirements: ['idContact' => '\d+'],
        methods: ['GET']
    )]
    #[OA\Response(
        response: 200,
        description: 'Subscription data',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: Subscription::class, groups: ['read']))
        )
    )]
    public function getSubscriptionByContactId(
        int $idContact,
        #[MapEntity(id: 'idContact')]
        ?Contact $contact = null
    ): JsonResponse {
        if (is_null($contact)) {
            throw $this->createNotFoundException(
                'No contact found for id '.$idContact
            );
        }

        $data = $this->serializer->normalize($contact->getSubscriptions(), null, ['groups' => 'read']);

        return $this->json($data);
    }

    /**
     * @throws \Exception
     */
    private function createSubscriptionFromDto(
        SubscriptionDto $subscriptionDto,
        ?Subscription $subscription = null
    ): void {
        if (is_null($subscription)) {
            $subscription = new Subscription();
        }

        $contact = $this->contactService->findById($subscriptionDto->getIdContact());
        $product = $this->productService->findById($subscriptionDto->getIdProduct());

        $subscription
            ->setContact($contact)
            ->setProduct($product)
            ->setBeginDate($subscriptionDto->getBeginDate())
            ->setEndDate($subscriptionDto->getEndDate())
        ;

        $this->subscriptionService->save($subscription);
    }
}
