<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Subscription;
use App\Exception\RepositoryException;
use App\Service\ExceptionFactory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * @extends ServiceEntityRepository<Subscription>
 */
class SubscriptionRepository extends ServiceEntityRepository
{
    public function __construct(
        private readonly ExceptionFactory $exceptionFactory,
        private readonly LoggerInterface $logger,
        ManagerRegistry $registry
    ) {
        parent::__construct($registry, Subscription::class);
    }

    /**
     * @throws \App\Exception\Exception
     */
    public function save(Subscription &$subscription): void
    {
        try {
            $this->getEntityManager()->persist($subscription);

            $this->getEntityManager()->flush();
        } catch (\Exception $exception) {
            $this->logger->warning('Error while persisting entity');
            $this->logger->warning($exception->getMessage());

            throw $this->exceptionFactory->create(
                RepositoryException::class,
                Response::HTTP_NOT_FOUND,
                'Error while persisting entity',
                'Not saved',
                'Could not save entity'
            );
        }
    }

    /**
     * @throws \App\Exception\Exception
     */
    public function remove(Subscription $subscription): void
    {
        try {
            $this->getEntityManager()->remove($subscription);

            $this->getEntityManager()->flush();
        } catch (\Exception $exception) {
            $this->logger->warning('Error while removing entity');
            $this->logger->warning($exception->getMessage());

            throw $this->exceptionFactory->create(RepositoryException::class);
        }
    }

    public function findByContactAndProduct(int $contactId, int $productId): mixed
    {
        return $this->createQueryBuilder('s')
            ->innerJoin('s.contact', 'c')
            ->innerJoin('s.product', 'p')
            ->where('c.id = :contactId')
            ->andWhere('p.id = :productId')
            ->setParameter('contactId', $contactId)
            ->setParameter('productId', $productId)
            ->getQuery()
            ->getResult()
        ;
    }
}
