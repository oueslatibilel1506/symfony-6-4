SHELL := /bin/bash

# Define global variables
DOCKER_COMPOSE_EXEC := docker-compose exec php8
DOCKER_COMPOSE := docker-compose

# Define default rule
.DEFAULT_GOAL= list

define display-title
	@echo "${_BOLD}$1${_END}"
endef

# (icon, title, description)
define display-command
	@echo "$1: $2${_END}"
endef

# Display available tasks
list:
	$(call display-command, "php-lint", "run linter on php files")
	$(call display-command, "install", "intaller")
	$(call display-command, "tests", "run tests")

tests:
	$(DOCKER_COMPOSE_EXEC) bin/console doctrine:database:drop --force --env=test || true
	$(DOCKER_COMPOSE_EXEC) bin/console doctrine:database:create --env=test
	$(DOCKER_COMPOSE_EXEC) bin/console doctrine:migrations:migrate -n --env=test
	$(DOCKER_COMPOSE_EXEC) bin/console doctrine:fixtures:load -n --env=test
	$(DOCKER_COMPOSE_EXEC) php bin/phpunit $(MAKECMDGOALS)
# Run tests
install:
	$(DOCKER_COMPOSE) build
	$(DOCKER_COMPOSE) up -d
	$(DOCKER_COMPOSE_EXEC) composer u
	$(DOCKER_COMPOSE_EXEC) bin/console doctrine:database:drop --force
	$(DOCKER_COMPOSE_EXEC) bin/console doctrine:database:create
	$(DOCKER_COMPOSE_EXEC) bin/console doctrine:migrations:migrate -n
	$(DOCKER_COMPOSE) up -d
	$(DOCKER_COMPOSE_EXEC) bin/console doctrine:fixtures:load -n

# Run PHP linting
php-lint:
	$(DOCKER_COMPOSE_EXEC) tools/php-cs-fixer/vendor/bin/php-cs-fixer fix src --rules=@PSR1,@PSR2,@Symfony,@PhpCsFixer,@DoctrineAnnotation
	$(DOCKER_COMPOSE_EXEC) vendor/bin/phpstan analyse src -l max --memory-limit=256M

.PHONY: install list php-lint tests
