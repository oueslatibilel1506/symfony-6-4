# Project Symfony 6.4

## Introduction
This project is developed using Symfony 6.4 and PHP 8.3. The goal is to provide a robust and scalable web application framework.

## Dockerized Environment
The project is fully Dockerized, making it easy to set up and run. To launch the project, simply execute the following command:

## Makefile Integration
To simplify various tasks during development, we have utilized the `make` command.

By typing `make`, you can see a list of available commands to streamline your workflow.

### Available Commands
- `make install`: Sets up the project environment and dependencies.
- `make tests`: Executes unit tests to ensure code integrity.
- `make php-lint`: Performs code linting and checks for adherence to coding standards.

## Installation

To get started with the project, follow these steps:

1. Clone the repository:

```bash
$ git clone https://gitlab.com/oueslatibilel1506/symfony-6-4.git
```

2. Navigate to the project directory and run the installation command:

```bash
$ cd symfony-6-4
$ make install
```

If you encounter any issues during the installation process, please refer to the troubleshooting section in our documentation for solutions.

## How it works?

Our project utilizes Docker containers to create a modular and scalable development environment. Here's a brief overview of the Docker containers and their functionalities:

* `maria-db`: This container hosts the MySQL database for our application, allowing for efficient data storage and retrieval.
* `php8`: This container runs PHP-FPM and hosts our application codebase, ensuring seamless execution of PHP scripts.
* `nginx`: The Nginx container acts as our web server, routing incoming requests to the appropriate PHP scripts and serving static assets.
* `phpmyadmin`: This container provides a user-friendly interface for managing the MySQL database via PhpMyAdmin.

Additionally, we have structured our application to follow the MVC (Model-View-Controller) architecture, with each component residing in its respective container and communicating via defined interfaces.

## Usage

After installation, you can visit your application on the following URL:

http://localhost:9080

(and access to documentation on localhost:9080/api/doc)

### Use PhpMyAdmin
PhpMyAdmin is included in our project to simplify database management tasks. You can access PhpMyAdmin by visiting the following URL in your web browser:

http://localhost:8080

Once logged in, you can perform various database operations such as creating, editing, and deleting tables, executing SQL queries, and managing user permissions.

### Use Simple Commands
#### Execute Tests
```bash
$ make tests
```

Running make tests will trigger our unit tests suite, ensuring that our application functions correctly and reliably.

#### Code Quality
```bash
$ make php-lint
```

The make php-lint command performs code linting and checks for adherence to coding standards using PHPStan and PHP CS Fixer. It helps maintain code consistency and identifies potential bugs or issues early in the development process.