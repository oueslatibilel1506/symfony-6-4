<?php

declare(strict_types=1);

namespace App\Tests;

use App\Exception\RepositoryException;
use App\Repository\SubscriptionRepository;
use App\Service\ExceptionFactory;
use App\Entity\Subscription;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ManagerRegistry;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class SubscriptionServiceTest extends TestCase
{
    private Subscription $subscription;
    private EntityManager $entityManager;
    private LoggerInterface $logger;
    private ManagerRegistry $registry;
    private ExceptionFactory $exceptionFactory;

    protected function setUp(): void
    {
        $this->subscription = $this->createMock(Subscription::class);
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->registry = $this->createMock(ManagerRegistry::class);
        $this->exceptionFactory = $this->createMock(ExceptionFactory::class);

        parent::setUp();
    }

    public function testSaveThrowsException(): void
    {
        // Configure the mock EntityManager to throw an exception on persist
        $this->entityManager
            ->expects($this->once())
            ->method('persist')
            ->with($this->subscription)
            ->will(
                $this->throwException(
                    new Exception('Persist error')
                )
            )
        ;

        // Configure the ExceptionFactory mock to return a specific exception
        $repositoryException = new RepositoryException(
            'Could not save entity',
            Response::HTTP_NOT_FOUND
        );

        $this->exceptionFactory->expects($this->once())
            ->method('create')
            ->with(
                RepositoryException::class,
                Response::HTTP_NOT_FOUND,
                'Error while persisting entity',
                'Not saved',
                'Could not save entity'
            )
            ->willReturn($repositoryException)
        ;

        // Instantiate the SubscriptionRepository with mocks
        $this->subscriptionRepository = $this->getMockBuilder(SubscriptionRepository::class)
            ->setConstructorArgs(
                [
                    $this->exceptionFactory,
                    $this->logger,
                    $this->registry
                ]
            )
            ->onlyMethods(
                [
                    'getEntityManager'
                ]
            )
            ->getMock()
        ;

        // Configure the repository mock to return the mocked EntityManager
        $this->subscriptionRepository
            ->method('getEntityManager')
            ->willReturn($this->entityManager)
        ;

        // Expect the exception
        $this->expectException(RepositoryException::class);
        $this->expectExceptionMessage('Could not save entity');

        // Call the save method
        $this->subscriptionRepository->save($this->subscription);
    }
}
