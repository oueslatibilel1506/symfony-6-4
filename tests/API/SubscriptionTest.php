<?php

declare(strict_types=1);

namespace App\Tests\API;

use App\Entity\Subscription;
use App\Exception\Exception;
use App\Factory\ContactFactory;
use App\Factory\ProductFactory;
use App\Factory\SubscriptionFactory;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class SubscriptionTest extends WebTestCase
{
    use Factories, ResetDatabase;
    private const BASE_URL = '/api/subscription';

    private $client;

    protected function setUp(): void
    {
        parent::setUp();

        $this->client = static::createClient();
    }

    public function testCreateSubscription(): void
    {
        $contactId = ContactFactory::createOne()->object()->getId();
        $productId = ProductFactory::createOne()->object()->getId();

        $this->assertSame(
            0,
            SubscriptionFactory::repository()->count(),
            'Expected no subscriptions initially'
        );

        $this->client->request(
            Request::METHOD_POST,
            self::BASE_URL,
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'idContact' => $contactId,
                'idProduct' => $productId,
                'beginDate' => '2023-01-01 10:00',
                'endDate' => '2024-01-02 20:00',
            ])
        );

        $this->assertSame(
            Response::HTTP_CREATED,
            $this->client->getResponse()->getStatusCode(),
            'Expected response status 201 Created'
        );

        $this->assertCount(
            1,
            SubscriptionFactory::repository()->findByContactAndProduct($contactId, $productId),
            'Expected one subscription after creation'
        );
    }

    public function testCreateSubscriptionWithResourceNotFound(): void
    {
        $invalidContactId = 6000;

        // Assert that a user with a specific email does not exist
        $this->assertNull(SubscriptionFactory::repository()->find($invalidContactId));

        $this->client->request(
            Request::METHOD_POST,
            self::BASE_URL,
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'idContact' => $invalidContactId,
                'idProduct' => 1,
                'beginDate' => '2023-01-01 10:00',
                'endDate' => '2024-01-02 20:00',
            ])
        );

        $response = $this->client->getResponse();
        $this->assertSame(
            Response::HTTP_NOT_FOUND,
            $response->getStatusCode(),
            'Expected response status 404 Not Found'
        );
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame(
            'No entity found for id ' . $invalidContactId,
            $responseData['message'],
            'Expected not found message'
        );
    }

    public function testCreateSubscriptionWithInvalidData(): void
    {
        $this->client->request(
            Request::METHOD_POST,
            self::BASE_URL,
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'idContact' => "invalid id contact",
                'idProduct' => 1,
                'beginDate' => '2023-01-01 10:00',
                'endDate' => '2024-01-02 20:00',
            ])
        );

        $response = $this->client->getResponse();
        $this->assertSame(
            Response::HTTP_UNPROCESSABLE_ENTITY,
            $response->getStatusCode(),
            'Expected response status 422 Unprocessable Entity'
        );
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame(
            'This value should be of type int.',
            $responseData['message'],
            'Expected validation error message for idContact'
        );
    }

    public function testCreateSubscriptionWithMissingData(): void
    {
        $this->client->request(
            Request::METHOD_POST,
            self::BASE_URL,
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'idContact' => 1,
                'idProduct' => 1,
                'beginDate' => '2023-01-01 10:00',
            ])
        );

        $response = $this->client->getResponse();
        $this->assertSame(
            Response::HTTP_UNPROCESSABLE_ENTITY,
            $response->getStatusCode(),
            'Expected response status 422 Unprocessable Entity'
        );
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame(
            'This value should be of type DateTimeInterface.',
            $responseData['message'],
            'Expected validation error message for missing endDate'
        );
    }

    public function testDeleteSubscription(): void
    {
        $contact = ContactFactory::createOne()->object();
        $product = ProductFactory::createOne()->object();

        $subscription = SubscriptionFactory::createOne(
            [
                'contact' => $contact,
                'product' => $product
            ]
        )->object();

        $this->assertSame(1, SubscriptionFactory::repository()->count());

        $subscriptionId = $subscription->getId();

        $this->client->request(
            Request::METHOD_DELETE,
            self::BASE_URL . '/' . $subscriptionId
        );

        $this->assertSame(
            Response::HTTP_NO_CONTENT,
            $this->client->getResponse()->getStatusCode(),
            'Expected response status 204 No Content'
        );

        $this->assertNull(
            SubscriptionFactory::repository()->find($subscriptionId),
            'Expected subscription to be deleted'
        );
    }

    public function testDeleteSubscriptionWithInvalidSubscriptionId(): void
    {
        $subscriptionId = 1;
        $this->client->request(
            Request::METHOD_DELETE,
            self::BASE_URL . '/' . $subscriptionId,
        );

        $this->assertNull(ContactFactory::repository()->find($subscriptionId));

        $this->assertSame(
            Response::HTTP_NOT_FOUND,
            $this->client->getResponse()->getStatusCode(),
            'Expected response status 404 No Content'
        );

        $response = $this->client->getResponse();
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame(
            'No Subscription found for id ' . $subscriptionId,
            $responseData['message'],
            'Expected resource not found'
        );

    }


    /**
     * @throws Exception
     */
    public function testPutSubscription(): void
    {
        $contact = ContactFactory::createOne()->object();
        $products = ProductFactory::createMany(2);

        $firstProduct = reset($products)->object();
        $secondProductId = $contact->getId();

        $subscription = SubscriptionFactory::new(
            [
                'contact' => $contact,
                'product' => $firstProduct
            ]
        )->create()
        ->enableAutoRefresh();

        $subscriptionId = $subscription->getId();

        $this->assertInstanceOf(
            Subscription::class,
            SubscriptionFactory::repository()->find($subscriptionId)->object()
        );

        $beginDate = '2023-01-01 10:00';
        $endDate = '2024-01-02 20:00';

        $this->client->request(
            Request::METHOD_PUT,
            self::BASE_URL . '/' . $subscriptionId,
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'idContact' => $contact->getId(),
                'idProduct' => $secondProductId,
                'beginDate' => $beginDate,
                'endDate' => $endDate,
            ])
        );

        $this->assertSame(
            Response::HTTP_NO_CONTENT,
            $this->client->getResponse()->getStatusCode(),
            'Expected response status 204 No Content'
        );

        $this->assertSame(
            [
                $contact->getId(),
                $secondProductId,
                $beginDate,
                $endDate
            ],
            [
                $subscription->getContact()->getId(),
                $subscription->getProduct()->getId(),
                $subscription->getBeginDate()->format('Y-m-d H:i'),
                $subscription->getEndDate()->format('Y-m-d H:i'),
            ]
        );
    }

    public function testPutSubscriptionWithInvalidSubscriptionId(): void
    {
        $subscriptionId = 1;
        $this->client->request(
            Request::METHOD_GET,
            self::BASE_URL . '/' . $subscriptionId,
        );

        $this->assertNull(ContactFactory::repository()->find($subscriptionId));

        $this->assertSame(
            Response::HTTP_NOT_FOUND,
            $this->client->getResponse()->getStatusCode(),
            'Expected response status 404 No Content'
        );

        $response = $this->client->getResponse();
        $responseData = json_decode($response->getContent(), true);
        $this->assertSame(
            'No contact found for id ' . $subscriptionId,
            $responseData['message'],
            'Expected resource not found'
        );

    }

    public function testGetSubscriptionByContactId(): void
    {
        $labelProduct = 'Product 1';
        $contact = ContactFactory::createOne()->object();
        $product = ProductFactory::createOne(
            [
                'label' => $labelProduct,
            ]
        )->object();

        $subscription = SubscriptionFactory::new(
            [
                'contact' => $contact,
                'product' => $product,

            ]
        )
        ->create()
        ->enableAutoRefresh()
        ;

        $this->client->request(
            Request::METHOD_GET,
            self::BASE_URL . '/' . $contact->getId()
        );

        $response = $this->client->getResponse();
        $this->assertSame(
            Response::HTTP_OK,
            $response->getStatusCode(),
            'Expected response status 200 OK'
        );

        $responseData = json_decode($response->getContent(), true);

        $this->assertSame(
            [
                [
                    'id' => $subscription->getId(),
                    'beginDate' => $subscription->getBeginDate()->format('Y-m-d\TH:i:sP'), // ISO 8601 format
                    'endDate' => $subscription->getEndDate()->format('Y-m-d\TH:i:sP'), // ISO 8601 format
                    'product' => [
                        'id' => $product->getId(),
                        'label' => $labelProduct,
                    ],
                ],
            ],
            $responseData,
            'Expected API response'
        );

    }
}
